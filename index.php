<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Baza filmova</title>
    </head>
    <body>
        <div>
        <?php
        include("inc/header.html");
        include("inc/aside.html");

        if(!isset($_GET["link"]))
        $_GET["link"]="index";

        switch ($_GET["link"]) {

            case "film":
            include("inc/film.php");
            break;

            case "genre":
            include("inc/genre.php");
            break;

            case "schedule":
            include("inc/schedule.php");
            break;

            default:
            include("inc/film.php");
            break;

            }
        include("inc/footer.html"); ?>
        </div>
    </body>
</html>
